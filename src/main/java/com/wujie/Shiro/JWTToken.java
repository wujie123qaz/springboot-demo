package com.wujie.Shiro;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.apache.shiro.authc.AuthenticationToken;

/**
 *重写获取token
 */
@NoArgsConstructor
@AllArgsConstructor
public class JWTToken implements AuthenticationToken {
    //密钥，在这里是表示用JWT技术生成的口令
    private String token;
    //
    private Boolean rememberMe;

    public JWTToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return this.token;
    }

    @Override
    public Object getCredentials() {
        return this.token;
    }



    public boolean isRememberMe() {
        return  rememberMe;
    }


    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

}

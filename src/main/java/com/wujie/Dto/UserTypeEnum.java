package com.wujie.Dto;

/**
 * 账号类型，统一用枚举表示，方便管理
 */
public enum UserTypeEnum {
    ADMIN("admin"),STUDENT("student"),TEACHER("teacher");

    private String usertype;

    UserTypeEnum(String usertype) {
        this.usertype = usertype;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }
}

package com.wujie.Mapper;

import com.wujie.Dto.UserInfo;
import com.wujie.Pojo.Admin;
import jdk.jfr.Percentage;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdminMapper {
    //登录验证
    Admin Login(@Param("username") String userID, @Param("password") String password);
    //获取管理员信息
    Admin getAdmin(@Param("username") String userID);
    //修改密码
    Integer updatePassword(@Param("ano") String ano , @Param("password") String newPassword);

    //获取管理员信息，根据传入的账号进行查询，若值为null则查询全部
    List<UserInfo> getAdmins(@Param("ano") String userID);

    //更改管理员账号的状态（isdelete）
    Integer updateStatus(@Param("isdelete") Integer status, @Param("ano") String userID);

    //添加管理员用户
    Integer insertAdminOne(UserInfo userInfo);

    //删除管理员用户
    Integer deleteAdminOne(UserInfo userInfo);

    //更新用户信息
    Integer updateAdminOne(UserInfo userInfo);
}

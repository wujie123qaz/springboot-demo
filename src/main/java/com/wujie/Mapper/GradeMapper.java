package com.wujie.Mapper;


import com.wujie.Pojo.Grade;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface GradeMapper {
    int deleteByPrimaryKey(@Param("id") Integer id, @Param("name") String name);

    int insert(Grade record);

    Grade selectByPrimaryKey(@Param("id") Integer id, @Param("name") String name);

    List<Grade> selectAll();

    int updateByPrimaryKey(Grade record);

    /**
     * 根据年级编号、年级名称动态获取数据，已过滤isdelete为1的数据
     */
    List<Grade> getGrade(@Param("gno") String gno, @Param("name") String name);

    /**
     * 根据年级编号、年级名称动态获取数据，包括isdelete为1的数据
     */
    List<Grade> getGradeAll(@Param("gno") String gno, @Param("name") String name);

    /**
     * 根据年级编号与名称进行更改
     */
    Integer updateByGnoAndName(Grade grade);

    /**
     * 根据年级编号更改
     */
    Integer updateByGno(Grade grade);


    /**
     * 根据年级编号和名称删除年级信息，实际是更改isdelete = 1;或者用于年级信息的状态更改
     */
    Integer deleteGrade(Grade grade);

}
package com.wujie.Aop;

import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class LoginRealmAspect {

    //切入点
    @Pointcut("execution(* com.wujie.Shiro.LoginRealm.*(..))")
    public void pointcut(){}

    @AfterThrowing(pointcut = "pointcut()",throwing = "e")
    public void  afterThrowing(Throwable e){
        System.out.println("LoginRealm 校验异常："+e);
    }
}

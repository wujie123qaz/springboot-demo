package com.wujie.service.serviceImpl;

import com.wujie.Dto.UserInfo;
import com.wujie.Pojo.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.wujie.Mapper.AdminMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class AdminServiceImpl  {
    @Autowired
    private AdminMapper adminMapper;
    /**
     *登录验证
     */
    public Map<String,Object> loginForm(String userID , String password){
        Map<String,Object> map = new HashMap<>();//存储验证结果
        Admin admin = adminMapper.Login(userID,password);
        System.out.println(admin); //调试-查看查询的结果
        if (admin != null && admin.getIsdelete() != 1 ){
            map.put("status",true);
        }else {
            if(admin != null && admin.getIsdelete() == 1){
                map.put("status", false);
                map.put("msg", "账号已删除");
            }else {
                map.put("status", false);
                map.put("msg", "账号密码错误或者不存在");
            }
        }
        System.out.println("map:"+map);//调试
        return map;
    }
    /**
     * 获取管理员信息
     */
    public Admin getAdmin(String userID){
        return  adminMapper.getAdmin(userID);
    }
    /**
     * 修改密码
     */
    public int updatePassword(String ano , String newPassword){
        return adminMapper.updatePassword(ano, newPassword);
    }
    /**
     * 根据账号进行查询，若账号为空，则查询全部账号
     */
    public List<UserInfo> getAdmins(String ano){
        List<UserInfo> admins = adminMapper.getAdmins(ano);
        return admins;
    }
    /**
     * 修改帐号状态
     *
     */
    public Integer updateStatus(String userID , Integer isdelete){
        return adminMapper.updateStatus(isdelete,userID);
    }

    /**
     *
     * 添加单个管理员用户
     */
    public Map<String,Object> insertAdminOne(UserInfo userInfo){
        Map<String, Object> map =new HashMap<>();
        Integer res = adminMapper.insertAdminOne(userInfo);
        if(res == 1){//影响的记录数为1则添加单条记录成功
            map.put("success",true);
            map.put("msg","添加成功");
        }else {
            map.put("success",false);
            map.put("msg","添加失败");
        }
        return map;
    }

    /**
     *删除单个管理员用户
     */
    public Map<String,Object> deleteAdminOne(UserInfo userInfo){
        Map<String, Object> map =new HashMap<>();
        Integer res = adminMapper.deleteAdminOne(userInfo);
        if(res == 1){//影响的记录数为1则删除单条记录成功
            map.put("success",true);
            map.put("msg","删除成功");
        }else {
            map.put("success",false);
            map.put("msg","删除失败");
        }
        return map;
    }

    /**
     * 修改单个用户信息
     */
    public Map<String,Object> updateAdminOne(UserInfo userInfo){
        Map<String, Object> map =new HashMap<>();
        Integer res = adminMapper.updateAdminOne(userInfo);
        if(res == 1){//影响的记录数为1则修改单条记录成功
            map.put("success",true);
            map.put("msg","更新信息成功");
        }else {
            map.put("success",false);
            map.put("msg","更新信息失败");
        }
        return map;
    }



}

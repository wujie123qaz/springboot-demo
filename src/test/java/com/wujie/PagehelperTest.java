package com.wujie;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import com.wujie.Mapper.GradeMapper;
import com.wujie.Pojo.Grade;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


@SpringBootTest
public class PagehelperTest {

    @Autowired
    GradeMapper gradeMapper;

    /**
     * 分页插件测试
     */
    @Test
    public void PagehelperCacheTest(){
        PageHelper.startPage(1,1);
        List<Grade> list = gradeMapper.selectAll();
        PageInfo<Grade> pageInfo = new PageInfo<>(list);
        System.out.println(pageInfo);
    }
}
